#!/usr/bin/python3

from requests import get, Session
from json import dumps
from colorama import Fore, Back, Style
from argparse import ArgumentParser, RawTextHelpFormatter

from requests.exceptions import ConnectionError, MissingSchema

agents = {
    "Firefox" : {
        'default': 70,
        70: "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0",
        57: "Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0"

    },
    "Chrome": { 
        'default': 78,
        59: "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36",
        78: "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36"

    },
    'IE': {
        'default': 11,
        11: "Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko",
        8: "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)",
        6: "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)"
    },
    'Edge': {
        17: "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/17.17134",
    },
    'Safari': {
        'default': 605,
        605 :"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Safari/605.1.15"
    },
    'Curl': {
        'default': 7,
        7: 'curl/7.37.0'
    },
    'Outlook': {
        'default': 15, 
        15: "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.3; .NET4.0C; .NET4.0E; Microsoft Outlook 15.0.5101; ms-office; MSOffice 15)"
    }
}

headers = {
    'User-Agent': agents['Firefox'][70]
}

proxies = {
    'http': 'http://127.0.0.1:3126',
    'https': 'http://127.0.0.1:3126'
}

red1 = Fore.WHITE + 'R' + Fore.RED
red2 = Fore.WHITE + 'E' + Fore.RED
red3 = Fore.WHITE + 'D' + Fore.RED

logo = Fore.RED + """
           _.====.._
         ,:._       ~-_
             `\        ~-_
              |    """+red1+"""     `.
             ,/  S """+red2+""" A     ~-_
    -..__..-''     """+red3+"""           ~~--..js...--
""" + Fore.RESET

def fix_len(string, length=10, left=False):
    string = str(string)
    if len(string) > length:
        return string[:length]
    else:
        for i in range(length - len(string)):
            if left:
                string = ' ' + string
            else:
                string += ' '
        return string


def process_url(url, ua, proxies=None, follow=False, max=1):

    # Prep session
    session = Session()
    session.max_redirects = max
    get = session.get 
    if not proxies:
        proxies = {}

    headers = {
        'Accept': 'text/html, */*',
        'User-Agent': ua
    }

    # Check
    try:

        resp = get(url, proxies=proxies, headers=headers, allow_redirects=follow)
        return resp
    except ConnectionError as ce:
        print('   ', Fore.RED + 'Connection Error' + Fore.RESET, ce.__dict__['request'].__dict__['url'])
    except MissingSchema:
        print('   ', Fore.RED + 'Missing schema' + Fore.RESET, 'Trying http://')
        return process_url('http://'+url, ua, proxies=proxies, follow=follow, max=max)
        
    except Exception as ex:
        print('   ', Fore.RED + fix_len('Exception', 16) + Fore.RESET, ex, ex.__class__.__name__)
        print('   ', fix_len('Requested',16), ex.__dict__['request'].__dict__['url'])
        print('   ', fix_len('Next',16), ex.__dict__['response'].__dict__['headers']['Location'])
        response = ex.__dict__['response']
        return response

    
def process_response(response):
    
    if response:
        if response.ok:
            result = response.url
            
            # Content hit
            if response.status_code in range(199,299):
                if 'Location' in response.headers:
                    result = response.headers['Location']
                status = Fore.GREEN + fix_len('Reached',  16)+ Fore.RESET
                reason = fix_len(response.reason, 20)
                print('   ', status, response.status_code, reason, '>', result)


            # Redirects
            elif response.status_code in range(300,399):
                if 'Location' in response.headers:
                    result = response.headers['Location']

                status = Fore.MAGENTA + fix_len('Redirect',  16)+ Fore.RESET
                reason = fix_len(response.reason, 20)
                print('   ', status, response.status_code, reason, '>', result)

            elif response.status_code in range(400,499):
                if 'Location' in response.headers:
                    result = response.headers['Location']

                status = Fore.MAGENTA + fix_len('Fail',  16)+ Fore.RESET
                reason = fix_len(response.reason, 20)
                print('   ', status, response.status_code, reason, '>', result)

            # Check request redirects
            if 'history' in response.__dict__.keys():
                for h in response.history:
                    status = Fore.BLUE + fix_len('HISTORY',  16)+ Fore.RESET
                    reason = fix_len(h.reason, 20)
                    print('   ', status, h.status_code, reason, '>', h.headers['Location'])


        else:
            print('F')
            # Missing, Forbidden etc

            if 'Location' in response.headers:
                result = response.headers['Location']
            status = Fore.CYAN + fix_len('ClientError',  16)+ Fore.RESET
            reason = fix_len(response.reason, 20)
            print('   ', status, response.status_code, reason, '>', result)

            print('Fail', response.status_code, response.reason)

    else:
        if 'Location' in response.headers:
            result = response.headers['Location']

            status = Fore.MAGENTA + fix_len('Fail',  16)+ Fore.RESET
            reason = fix_len(response.reason, 20)
            print('   ', status, response.status_code, reason, '>', result)

        pass
        #print('Error. No response')


parser = ArgumentParser(prog='redsea', description=logo, formatter_class=RawTextHelpFormatter, epilog='Happy hunting')
parser.add_argument('url', help='Provide '+Fore.RED+'url'+Fore.RESET+' to check for\nUA dependent redirections')
parser.add_argument('-a', '--all', action='store_true', help='Check all configured UserAgents')
parser.add_argument('-f', '--follow', action="store_true", help='Follow redirections')
parser.add_argument('-m', '--max', type=int, dest='max', help='Maximum number of allowed redirections')
parser.add_argument('-p', '--use-proxy', dest='proxy', action='store_true', help='Use proxy')

import sys
if len(sys.argv)==1:
    parser.print_help(sys.stderr)
    sys.exit(1)

args = parser.parse_args()


if args.url:

    url = args.url
    redirects = False
    max_redirects = 1
    proxies = {}
    if args.proxy:
        proxies = {
            'http': 'http://127.0.0.1:3126',
            'https': 'http://127.0.0.1:3126'
        }

    if args.follow:
        redirects = True
        if args.max:
            max_redirects = args.max

    # process all UA
    for agent in agents:
        for version in agents[agent]:
            if args.all:
                if version == 'default':
                    continue
                else:

                    print('\n[+]', Fore.LIGHTYELLOW_EX + fix_len(agent,7) +Fore.RESET, Fore.YELLOW, str(version) + Fore.RESET)

                    resp = process_url(url, agents[agent][version], proxies=proxies, follow=redirects, max=max_redirects)

                    process_response(resp)

            elif version == 'default':
                print('\n[+]', Fore.LIGHTYELLOW_EX + fix_len(agent,7) +Fore.RESET, Fore.YELLOW +  str(agents[agent][version]) + Fore.RESET)

                resp = process_url(url, agents[agent][agents[agent][version]], proxies=proxies, follow=redirects, max=max_redirects)

                process_response(resp)
            
